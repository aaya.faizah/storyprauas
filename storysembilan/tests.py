from django.test import TestCase

# Create your tests here.

from django.test import TestCase, Client
from django.apps import apps
from django.urls import resolve, reverse

from .views import storysembilan, register, signin, signout
from .apps import StorySembilanConfig

# Create your tests here.
class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(StorySembilanConfig.name, 'storysembilan')
        self.assertEqual(apps.get_app_config('storysembilan').name, 'storysembilan')

class TestUrls(TestCase):
    def setUp(self):
        self.client = Client()
        
    def test_main_url(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)
    
    def test_login_url(self):
        response = Client().get('/storysembilan/signin/')
        self.assertEquals(response.status_code, 200)

    def test_register_url(self):
        response = Client().get('/storysembilan/register/')
        self.assertEquals(response.status_code, 200)

class TestFunctions(TestCase):
    def test_main_function(self):
        found = resolve('/storysembilan/')
        self.assertEqual(found.func, storysembilan)

    def test_login_function(self):
        found = resolve('/storysembilan/signin/')
        self.assertEqual(found.func, signin)

    def test_register_function(self):
        found = resolve('/storysembilan/register/')
        self.assertEqual(found.func, register)
    
    def test_signout_function(self):
        found = resolve('/storysembilan/signout/')
        self.assertEqual(found.func, signout)

class TestingViews(TestCase):
    def setUp(self):
        self.client = Client()

    def test_storysembilan(self):
        response = self.client.get("/storysembilan/")
        self.assertEqual(response.status_code, 302)

class TestingHTML(TestCase):
    def test_register_available(self):
        response = self.client.get("/storysembilan/register/")
        self.assertTemplateUsed(response, 'register.html')
    
    def test_signin_available(self):
        response = self.client.get("/storysembilan/signin/")
        self.assertTemplateUsed(response, 'signin.html')