from django.urls import path
from django.contrib import admin

from . import views

app_name = 'storysembilan'

urlpatterns = [
    path('', views.storysembilan, name='storysembilan'),
    path('register/', views.register, name='register'),
    path('signin/', views.signin, name='signin'),
    path('signout/', views.signout, name='signout'),
]