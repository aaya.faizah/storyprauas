from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required


# Create your views here.
from .forms import Register

@login_required(login_url="signin/")
def storysembilan(request):
    return render(request, 'storysembilan.html')

def register(request):
    if request.user.is_authenticated:
        return redirect('/storysembilan/')
    else:
        form = Register()
        if request.method == 'POST':
            form = Register(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, user + " was created! Please log in to continue.")
                return redirect('/storysembilan/signin/')

        response = {
            'form' : form
        }

        return render(request, "register.html", response)

def signin(request):
    if request.user.is_authenticated:
        return redirect('/storysembilan/')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('/storysembilan/')
            
            else:
                messages.info(request, 'Wrong password/username!')

        response = {}
        return render(request, "signin.html", response)

def signout(request):
    logout(request)
    return redirect('/storysembilan/signin/')