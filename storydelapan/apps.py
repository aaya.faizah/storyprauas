from django.apps import AppConfig


class StoryDelapanConfig(AppConfig):
    name = 'storydelapan'
