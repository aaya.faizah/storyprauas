from django.test import TestCase

# Create your tests here.

from django.test import TestCase, Client
from django.apps import apps
from django.urls import resolve, reverse

from .views import storydelapan
from .apps import StoryDelapanConfig

#Create your tests here.
class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(StoryDelapanConfig.name, 'storydelapan')
        self.assertEqual(apps.get_app_config('storydelapan').name, 'storydelapan')

class TestUrls(TestCase):
    def setUp(self):
        self.client = Client()
        
    def test_main_url(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_main_function(self):
        found = resolve('/storydelapan/')
        self.assertEqual(found.func, storydelapan)

class TestingViews(TestCase):
    def setUp(self):
        self.client = Client()

    def test_storydelapan(self):
        response = self.client.get("/storydelapan/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "storydelapan.html")

    def test_search_form(self):
        response = self.client.get('/storydelapan/')
        self.assertContains(response, 'find your book')

    def test_search_function(self):
        response = self.client.get('/storydelapan/search?key=harry')
        self.assertEqual(response.status_code, 200)

class TestingHTML(TestCase):
    def test_storydelapan_available(self):
        response = self.client.get("/storydelapan/")
        self.assertTemplateUsed(response, 'storydelapan.html')