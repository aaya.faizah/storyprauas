// $(document).ready(() => {
//     // })

$(document).ready(function () {
    $("#search").on("keyup", function (e) {
        var key = e.currentTarget.value.toLowerCase()
        console.log(key)
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + key,
            success: function (search) {
                $('#listofbooks').html('')
                var result = '<tr>';
                for (var i = 0; i < search.items.length; i++) {
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='max-width:15vw' src='" +
                        search.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + search.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + search.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + search.items[i].volumeInfo.publisher + "</td>"
                }
                $('#listofbooks').append(result);
            },
        })
    });

    $('#search').val('')
        $.ajax({
            method: 'GET',
            url: 'search?key=harry+potter',
            success: function(search){
                $('#listofbooks').html('')
                var result = '<tr>';
                for (var i = 0; i < search.items.length; i++) {
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='max-width:15vw' src='" +
                        search.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + search.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + search.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + search.items[i].volumeInfo.publisher + "</td>"
                }
                $('#listofbooks').append(result);
            }
        }) 
});
