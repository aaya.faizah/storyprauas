from django.urls import path
from django.contrib import admin

from . import views

app_name = 'storydelapan'

urlpatterns = [
    path('', views.storydelapan, name='storydelapan'),
    path('search', views.search),
]