$(document).ready(function () {
    // accordion main functionality
    $(".box-title").click(function () {
        // close box, remove active class on self-click
        if ($(this).parent().next(".box-content").hasClass("opened")) {
            $(this).parent().next(".box-content").removeClass("opened").slideUp()
        }
        // open box, add active class on click
        else {
            $(".individual-box .box-content").slideUp()
            $(this).parent().next(".box-content").addClass("opened")
            $(this).parent().next(".box-content").slideDown()
        }
    })

    // move boxes up and down accordingly
    $(".go-up").click(function(){
        var currentBox = $(this).parent().parent().parent();
        currentBox.insertBefore(currentBox.prev());
    })

    $(".go-down").click(function(){
        var currentBox = $(this).parent().parent().parent();
        currentBox.insertAfter(currentBox.next());
    })
})