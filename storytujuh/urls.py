from django.urls import path
from django.contrib import admin

from . import views

app_name = 'storytujuh'

urlpatterns = [
    path('', views.storytujuh, name='storytujuh'),
]