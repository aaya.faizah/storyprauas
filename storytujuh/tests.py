from django.test import TestCase

# Create your tests here.

from django.test import TestCase, Client
from django.apps import apps
from django.urls import resolve, reverse

from .views import storytujuh
from .apps import StoryTujuhConfig

#Create your tests here.
class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(StoryTujuhConfig.name, 'storytujuh')
        self.assertEqual(apps.get_app_config('storytujuh').name, 'storytujuh')

class TestUrls(TestCase):
    def setUp(self):
        self.client = Client()
        
    def test_main_url(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

class TestingViews(TestCase):
    def setUp(self):
        self.client = Client()

    def test_storytujuh(self):
        response = self.client.get("/storytujuh/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "storytujuh.html")

class TestingHTML(TestCase):
    def test_storytujuh_available(self):
        response = self.client.get("/storytujuh/")
        self.assertTemplateUsed(response, 'storytujuh.html')